//EQUIPE LOST

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int get_int(FILE * file) {
    int ler;
    fscanf(file, "%d", &ler);
    if (feof(file)) {
	fprintf(stderr, "EOF at size read!\n");
	exit(EXIT_FAILURE);
    }
    return ler;
}

int get_ll(FILE * file) {
    long long ler;
    fscanf(file, "%lld", &ler);
    if (feof(file)) {
	fprintf(stderr, "EOF at size read!\n");
	exit(EXIT_FAILURE);
    }
    return ler;
}

//caution -=48
char get_char(FILE * file) {
    char ler;
    ler = fgetc(file);
    if (ler == '\n')
	ler = fgetc(file);
    //ler -= 48;
    return ler;
}

//então, primeiro acha um range min e max, daí só depois trabalha dentro desse range
//para não desperdiçar CPU
//os resultados corretos são somente os quadrados perfeitos e o resultado de 1 foi propositalmente deixado como 0.5.
float raiz_xunxada(float n){
	float f_min, f_max;
	int i_min, i_max;
	const int i_n = (int) n;

	if(n==1.0)
		return 0.5;

	i_min=1;

	int i_raiz=i_min;
	while ((i_raiz * i_raiz) < i_n){
		i_raiz = i_raiz << 1;
	}

	if(i_raiz != i_min){
		i_max = i_raiz;
		i_min = i_raiz >> 1;
	}else{
		if(i_raiz > i_n)
			i_min = 0;
		else
			i_min = i_raiz;
		i_max = i_min << 1;
		if(i_max > i_n)
			i_max = i_n;
	}
	f_min = (float) i_min;
	f_max = (float) i_max;
	//printf("%f(%d)->%f(%d)\n", f_min, i_min, f_max, i_max);
	float raiz = f_min;
	float potencia;
	while(1){
		potencia = raiz * raiz;
		if(potencia == n)
			break;
		if(raiz==f_max){
				potencia = n+1.0;
		}
		if (potencia > n){
			raiz--;
			//aqui está o xunxo, adiciono meio para saber que não é exato...
			raiz+=0.5;
			break;
		}
		raiz++;
	}
	return raiz;
}

typedef struct quadrado_perfeito {
	int n;
	int cont;
	struct quadrado_perfeito * prox;
} s_q_p;

int possui_divisor_qp(int n, s_q_p * qps){
	float f_n = (float) n;
	s_q_p * it;
	int possui = 0;
	it = qps;
	while(it != NULL){
		float f_qp = (float) it->n;
		float res = f_n/f_qp;
		if( ((float)((int)res)) == res){
		////	printf("Divisor de %d é %d\n",n,qps->n);
		////	printf("\t%f < %f\n", ((float)((int)res)), res);
			possui = 1;
			break;
		}
		it=it->prox;
	}
	return possui;
}

void incluir_qp(s_q_p ** p_qps, int n, int cont){
	s_q_p * qps = *p_qps;
	s_q_p * new = (s_q_p *) malloc(sizeof(s_q_p));
	new->n = n;
	new->cont = cont;
	new->prox = NULL;
	if(qps == NULL){
		*p_qps = new;
	}else{
		s_q_p * last = qps;
		while (last->prox != NULL){
			if(last->n == n){
				//já existe na lista
				free(new);
				return;
			}
			last = last->prox;
		}
		last->prox = new;
	}
}

void apagar_qps(s_q_p ** p_qps){
	s_q_p * to_free = *p_qps;
	s_q_p * prox;
	while(to_free !=NULL){
		prox = to_free->prox;
		free(to_free);
		to_free = prox;
	}
	*p_qps = NULL;
}

int possui_qp(int n, s_q_p * qps){
	while(qps != NULL){
		if(qps->n == n)
			return 1;
		qps=qps->prox;
	}
	return 0;
}

int main(int argc, char **argv) {
    //FILE *file = fopen(argv[1], "r");
    //FILE *saida = fopen("saida", "w");
    //if (file == NULL /*|| saida == NULL*/) {
//	fprintf(stderr, "Impossivel abrir arquivo!");
//	exit(EXIT_FAILURE);
  //  }
	int T;
	scanf("%d", &T);
   
	long long i;//contador de instancia
	long long j;//número sendo analisado
	long long k;//inst
	int l=0;//apenas para armazenar os qps (inútil para esse exercício)
	long long instancia;
	float raiz;
	s_q_p * qps = NULL;
	for (int i = 0ll; i < T; i++){
		scanf("%lld", &instancia);
		j = 1ll;
		k=0ll;
		while(1){
			if(possui_qp(j,qps)){
				//cache? :P
				j++;
				continue;
			}
			raiz = raiz_xunxada(j);
			//raiz = sqrt(j);
			if(j==1ll)
				raiz = 0.5;
			//1.0<1.75
			if(((float)((int)raiz)) < raiz){
				if(!possui_divisor_qp(j, qps)){
					//não é quadrado perfeito e não é divisível por um quadrado perfeito
					k++;
					if(k==instancia)
						break;
				}//else
					//printf("Numero %d eliminado por ter divisor qp.\n", j);
			}else{
				l++;
				if(j>1ll){
					//quadrado perfeito != 1
					incluir_qp(&qps, j, l);
					if(qps==NULL){
						fprintf(stderr,"Lista nula.\n");
						exit(1);
					}
					fflush(stdout);
				}
			}
			j++;
		}
		printf("%d\n",j);
		fflush(stdout);
	}
 	apagar_qps(&qps);
    //fclose(saida);
    //fclose(file);

    return EXIT_SUCCESS;
}
